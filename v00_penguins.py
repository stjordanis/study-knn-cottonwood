from cottonwood.structure import Structure
from knn_a import KnnClassifier
from penguins import Data

model = Structure()
model.add(Data(), "data")
model.add(KnnClassifier(), "knn")

model.connect("data", "knn")
model.connect("data", "knn", i_port_tail=1, i_port_head=1)

for i in range(100):
    model.forward_pass()
