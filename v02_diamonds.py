"""
This code makes use of the Cottonwood machine learning framework.
Get it at https://e2eml.school/cottonwood
"""
import numpy as np
import matplotlib.pyplot as plt
from cottonwood.structure import Structure
from knn_b import KNN
from diamonds import Data

k = 5
n_reps = 1000
n_iter = 1000
difference = np.zeros(n_iter)

for i_rep in range(n_reps):

    print(f"rep {i_rep}", end="\r")
    model = Structure()
    model.add(Data(), "data")
    model.add(KNN(k=k, is_classifier=False, weight_adjustment_interval=1e10), "knn")

    model.connect("data", "knn")
    model.connect("data", "knn", i_port_tail=1, i_port_head=1)
    for i_iter in range(n_iter):
        model.forward_pass()
        try:
            difference[i_iter] += np.abs(model.blocks["knn"].target_label -
                model.blocks["knn"].forward_out)
        except TypeError:
            # Initially some of the estimates are None. Ignore these
            pass

mean_difference = difference / n_reps
fig = plt.figure()
ax = fig.gca()
ax.plot(mean_difference[k + 1:], color="#04253a")
ax.set_title(f"k-NN with Diamond Prices, k = {model.blocks['knn'].k}")
ax.set_xlabel("Iteration")
ax.set_ylabel("Mean price difference in $USD")
ax.grid()
plt.savefig("learning_curve_diamonds.png", dpi=300)
plt.show()
