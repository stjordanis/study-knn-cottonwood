import numpy as np

epsilon = 1e-3
weights_filename = "feature_weights.csv"
# The size of adjustment to a weight.
# Should be greater than zero. Best results if it's not much bigger than one.
weight_adjustment_amount = .5


class KnnClassifier:
    """
    An online version of the k-Nearest Neighbors for classification.
    "Online" here means it handles new data points one at a time
    and incrementally updates the model each time.
    """
    def __init__(self, k=5, weight_adjustment_interval=1e10):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

        self.k = int(k)
        self.n_features = None
        self.max_points = int(1e4)
        self.n_points = int(0)
        self.points = None
        self.labels = None

        # A list of all the categories seen so far.
        self.categories = []

        self.weights = None
        self.weight_adjustment_interval = weight_adjustment_interval
        self.weight_adjustment_timer = 0

    def initialize(self):
        self.n_features = int(self.target.size)
        self.points = np.zeros((self.max_points, self.n_features))
        self.labels = [None] * self.max_points
        self.weights = load_weights(self.n_features)

    def __str__(self):
        str_parts = [
            "k-nearest neighbors classifier",
            f"k: {self.k}",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        """
        The algorithm handles one example at a time.
        Whatever shape the forward input has, it will be assumed to
        be a set of feature values corresponding to the current
        example and will be flattened.
        """
        self.forward_in = forward_in
        # This is the case where both a feature array and a label are
        # passed in as (feature_array, label)
        if type(forward_in) == tuple:
            self.target = np.array(self.forward_in[0]).ravel()
            self.target_label = self.forward_in[1]
        else:
            # This is the case where only a feature array is passed in.
            self.target = np.array(self.forward_in).ravel()
            self.target_label = None

        if self.n_features is None:
            self.initialize()

        # Estimate the correct class for the new point
        i_nearest, distances = find_k_nearest(
            self.points[:self.n_points, :],
            self.target,
            self.k,
            self.weights)
        if i_nearest is None:
            estimate = None
        else:
            estimate = self.estimate_target(i_nearest, distances)

        # Add the new point to the collection of previously observed points
        # if a target label was supplied.
        if self.n_points < self.max_points and self.target_label is not None:
            self.points[int(self.n_points), :] = self.target
            self.labels[int(self.n_points)] = self.target_label
            self.n_points += 1
            if self.target_label not in self.categories:
                self.categories.append(self.target_label)

        # At the frequency requested, try to improve the feature weights.
        self.weight_adjustment_timer += 1
        if self.weight_adjustment_timer >= self.weight_adjustment_interval:
            self.adjust_weights(
                self.points[:self.n_points, :],
                self.labels[:self.n_points],
                self.weights)

        # print(f"estimate {estimate}, target {self.target_label}")
        self.forward_out = estimate
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        return self.backward_out

    def estimate_target(self, indices, distances):
        # Handle the case where there aren't yet enough data points.
        if distances.size < self.k:
            return None

        # Create a ballot box for each category.
        predictions = np.zeros(len(self.categories))
        # Collect the votes from each neighbor.
        for i_index, i_label in enumerate(indices):
            label = self.labels[i_label]
            predictions[self.categories.index(label)] += (
                1 / (distances[i_index] + epsilon))
        # Tally them up.
        i_winner = np.argmax(predictions)
        estimated_label = self.categories[i_winner]
        return estimated_label

    def adjust_weights(self):
        """
        Randomly select a feature weight, and adjust it either up by 50%
        or down by 33%, at random.
        """
        i_change = np.random.randint(self.n_features)
        adjusted_weights = self.weights.copy()
        if np.random.sample() > .5:
            adjusted_weights[i_change] *= 1 + weight_adjustment_amount
        else:
            adjusted_weights[i_change] *= 1 / (1 + weight_adjustment_amount)

        # Find a score for the current weights set and compare it to the score
        # for the adjusted weights.
        baseline_score = self.loo_cv()
        temp_weights = self.weights
        self.weights = adjusted_weights
        new_score = self.loo_cv()
        self.weights = temp_weights

        # If the adjusted weights give better results,
        # swap them in and save them.
        if new_score > baseline_score:
            self.weights = adjusted_weights
            save_weights(adjusted_weights)

    def loo_cv(self):
        """
        Perform leave-one-out cross validation.
        """
        # Check for the case where the model hasn't gained enough experience
        # yet.
        if self.labels.size < self.k:
            return 0

        score = 0
        for i_point, label in self.labels:
            i_nearest, distances = find_k_nearest(
                self.points[self.n_points, :],
                self.points[i_point, :],
                self.k + 1,
                self.weights)
            if i_nearest is not None:
                # One of the best matches will almost certainly be the target
                # point. Find it and remove it.
                i_nearest = list(i_nearest)
                distances = list(distances)
                if i_point in i_nearest:
                    i_match = i_nearest.index(i_point)
                    del i_nearest[i_match]
                    del distances[i_match]
                i_nearest = np.array(i_nearest)
                distances = np.array(distances)

                estimate = self.estimate_target(i_nearest, distances)
                if estimate == label:
                    score += 1

        return score / self.labels.size


def save_weights(new_weights):
    """
    Save a set of feature weights to a file.

    Arguments
    new_weights, 1D Numpy array of floats
    """
    with open(weights_filename, "w") as f:
        for weight in new_weights:
            f.writeline(str(weight))


def load_weights(n_features):
    weights = []
    try:
        with open(weights_filename, "rt") as f:
            data_lines = f.readlines()
            for line in data_lines:
                weights.append(float(line))
            weights = np.array(weights)
    except FileNotFoundError:
        # Handle the case where the feature weights file
        # hasn't been created yet
        weights = np.ones(n_features)

    return weights


def find_k_nearest(points, target, k, weights):
    """
    Find the distance between the test point and each of the training points.
    Use the Manhattan distance, the sum of differences in each dimension.
    """
    if points.shape[0] <= k:
        return None, None

    distance = np.sum(
        np.abs(points - target[np.newaxis, :]) * weights[np.newaxis, :],
        axis=1)
    i_partitioned = np.argpartition(distance, k)
    i_top_k = i_partitioned[:k]
    distance_top_k = distance[i_top_k]
    return i_top_k, distance_top_k
