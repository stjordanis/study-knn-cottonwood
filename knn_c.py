import numpy as np

class KNN:
    """
    An online version of the k-Nearest Neighbors for classification.
    "Online" here means it handles new data points one at a time
    and incrementally updates the model each time.

    For a description of the k-NN algorithm, check out
    https://youtu.be/KluQCQtHTqk
    """
    def __init__(
        self,
        k=5,
        is_classifier=True,
        max_points=int(1e5),
        use_pretrained_weights=False,
        weight_adjustment_amount=1,
        weight_adjustment_interval=1e10,
        weight_adjustment_wait=1000,
        weights_filename="feature_weights.csv",
    ):
        """
        Arguments

        k, int
            The star of the show. How many neighbors will be used
            to make each estimate.

        is_classifier, boolean
            True: this instance of k-NN is going to be used for classification.
            False: it's going to be used for regression (estimating a
                floating point value).

        max_points, int
            The maximum number of data points that will be retained in the
            k-NN model.

        use_pretrained_weights, boolean
            Flag indicating whether to try to load the weights from
            a file.
        weight_adjustment_amount, float
            The size of adjustment to a weight.
            Should be greater than zero.
            Best results if it's not much bigger than one.

        weight_adjustment_interval, float
            How many iterations between trying to adjust weights.
            By default it's enormous and weights will bever be adjusted.
            If you'd like aggressive adaptation, you can set it to 1.

        weight_adjustment_wait, float
            How many iterations to wait until weight adjustments start to
            be attempted.

        weight_filename, str
            The name of the csv where the weights will be stored.
        """
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

        self.is_classifier = is_classifier
        self.k = int(k)
        self.n_features = None
        self.max_points = max_points
        self.n_points = int(0)
        self.points = None
        self.labels = []

        # The number of points used to estimate the variance.
        # This can be different than the number of points retained
        # in the model.
        self.n_points_var = 1
        self.feature_means = None
        self.sum_square_diffs = None
        self.stddevs = None

        self.use_pretrained_weights = use_pretrained_weights
        self.weights_filename = weights_filename
        self.weights = None
        self.weight_adjustment_interval = weight_adjustment_interval
        self.weight_adjustment_timer = -weight_adjustment_wait
        self.weight_adjustment_amount = weight_adjustment_amount

        # A list of all the categories seen so far.
        if self.is_classifier:
            self.categories = []

    def initialize(self):
        self.n_features = int(self.target.size)
        self.points = np.zeros((self.max_points, self.n_features))
        self.labels = [None] * self.max_points
        if self.use_pretrained_weights:
            self.weights = self.load_weights()
        else:
            self.weights = np.ones(self.n_features)
        self.sum_square_diffs = np.ones(self.n_features)
        self.stddevs = np.ones(self.n_features)
        self.feature_means = self.target.copy()
        self.previous_feature_means = self.target.copy()

    def __str__(self):
        str_parts = [
            "k-nearest neighbors classifier",
            f"k: {self.k}",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        """
        The algorithm handles one example at a time.
        Whatever shape the forward input has, it will be assumed to
        be a set of feature values corresponding to the current
        example and will be flattened.
        """
        self.forward_in = forward_in
        # This is the case where both a feature array and a label are
        # passed in as (feature_array, label)
        if type(forward_in) == tuple:
            self.target = np.array(self.forward_in[0]).ravel()
            self.target_label = self.forward_in[1]
        else:
            # This is the case where only a feature array is passed in.
            self.target = np.array(self.forward_in).ravel()
            self.target_label = None

        if self.n_features is None:
            self.initialize()

        self.update_variance_estimates()

        # Estimate the correct class for the new point
        i_nearest, distances = self.find_k_nearest()
        if i_nearest is None:
            estimate = None
        else:
            estimate = self.estimate_target(i_nearest, distances)

        # Add the new point to the collection of previously observed points
        # if a target label was supplied.
        if self.n_points < self.max_points and self.target_label is not None:
            self.points[int(self.n_points), :] = self.target
            self.labels[int(self.n_points)] = self.target_label
            self.n_points += 1
            if self.is_classifier:
                if self.target_label not in self.categories:
                   self.categories.append(self.target_label)

        # At the frequency requested, try to improve the feature weights.
        self.weight_adjustment_timer += 1
        if self.weight_adjustment_timer >= self.weight_adjustment_interval:
            self.adjust_weights()
            self.weight_adjustment_timer = 0

        self.forward_out = estimate
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        return self.backward_out

    def update_variance_estimates(self):
        """
        https://fanf2.user.srcf.net/hermes/doc/antiforgery/stats.pdf
        """
        self.n_points_var += 1
        self.previous_feature_means = self.feature_means
        self.feature_means = (
            (self.target + self.previous_feature_means *
            (self.n_points_var - 1)) /
            self.n_points_var)
        self.sum_square_diffs = (
            self.sum_square_diffs +
            (self.target - self.feature_means) *
            (self.target - self.previous_feature_means))
        self.stddevs = np.sqrt(self.sum_square_diffs / self.n_points_var)

    def find_k_nearest(self):
        """
        Find the distance between the test point and each of the training points.
        Use the Manhattan distance, the sum of differences in each dimension.
        """
        if self.n_points <= self.k:
            return None, None

        points = self.points[:self.n_points, :]
        distance = np.sum(
            np.abs(points - self.target[np.newaxis, :]) *
            (self.weights / self.stddevs)[np.newaxis, :],
            axis=1)
        i_partitioned = np.argpartition(distance, self.k)
        i_top_k = i_partitioned[:self.k]
        distance_top_k = distance[i_top_k]
        return i_top_k, distance_top_k

    def estimate_target(self, indices, distances):
        # Handle the case where there aren't yet enough data points.
        if distances.size < self.k:
            return None

        # Taking the log of the distance gives us a scale-independent
        # version of the distance. It will help kNN work well across
        # a wide range of sampling densities.
        log_distances = np.log(distances + 1)
        vote_weights = 1 / (log_distances + 1)

        if self.is_classifier:
            # kNN is expected to estimate a category.
            # Create a ballot box for each category.
            predictions = np.zeros(len(self.categories))

            # Collect the votes from each neighbor.
            for i_index, i_label in enumerate(indices):
                label = self.labels[i_label]
                predictions[self.categories.index(label)] += (
                    vote_weights[i_index])

            # Tally them up.
            i_winner = np.argmax(predictions)
            estimated_label = self.categories[i_winner]

        else:
            # kNN is expected to estimate a real value.
            total_votes = 0
            total_weights = 0
            for i_index, i_label in enumerate(indices):
                total_votes += self.labels[i_label] * vote_weights[i_index]
                total_weights += vote_weights[i_index]
            estimated_label = total_votes / total_weights
        return estimated_label

    def adjust_weights(self):
        """
        Randomly select a feature weight, and adjust it either up or down
        at random.
        """
        i_change = np.random.randint(self.n_features)
        adjusted_weights = self.weights.copy()
        if np.random.sample() > .5:
            adjusted_weights[i_change] *= 1 + self.weight_adjustment_amount
        else:
            adjusted_weights[i_change] *= (
                1 / (1 + self.weight_adjustment_amount))

        # Find a score for the current weights set and compare it to the score
        # for the adjusted weights.
        baseline_score = self.loo_cv()
        temp_weights = self.weights
        self.weights = adjusted_weights
        new_score = self.loo_cv()
        self.weights = temp_weights

        # If the adjusted weights give better results,
        # swap them in and save them.
        if self.is_classifier:
            if new_score > baseline_score:
                self.weights = adjusted_weights
                self.save_weights()
                print(f"Weights updated at iteration {self.n_points}")
        else:
            if new_score < baseline_score:
                self.weights = adjusted_weights
                self.save_weights()
                print(f"Weights updated at iteration {self.n_points}")

    def loo_cv(self):
        """
        Perform leave-one-out cross validation.
        """
        # Check for the case where the model hasn't gained enough experience
        # yet.
        if self.n_points < self.k + 2:
            return 0

        score = 0
        # for i_point in rangeself.labels[:int(self.n_points)]:
        for i_point in range(self.n_points):
            label = self.labels[i_point]
            self.k += 1
            i_nearest, distances = self.find_k_nearest()
            self.k -= 1
            if i_nearest is not None:
                # One of the best matches will almost certainly be the target
                # point. Find it and remove it.
                i_nearest = list(i_nearest)
                distances = list(distances)
                if i_point in i_nearest:
                    i_match = i_nearest.index(i_point)
                    del i_nearest[i_match]
                    del distances[i_match]
                i_nearest = np.array(i_nearest)
                distances = np.array(distances)
                estimate = self.estimate_target(i_nearest, distances)
                if self.is_classifier:
                    if estimate == label:
                        score += 1
                else:
                    score += np.abs(estimate - label)

        return score / self.n_points

    def save_weights(self):
        """
        Save a set of feature weights to a file.

        Arguments
        new_weights, 1D Numpy array of floats
        """
        with open(self.weights_filename, "w") as f:
            for weight in self.weights:
                f.write(str(weight) + "\n")

    def load_weights(self):
        try:
            with open(self.weights_filename, "rt") as f:
                data_lines = f.readlines()
                weights = []
                for line in data_lines:
                    weights.append(float(line))
                weights = np.array(weights)
                assert weights.size == self.n_features
        except (FileNotFoundError, AssertionError):
            # Handle the case where the feature weights file
            # hasn't been created yet
            weights = np.ones(self.n_features)

        return weights
