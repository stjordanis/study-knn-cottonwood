"""
This code makes use of the Cottonwood machine learning framework.
Get it at https://e2eml.school/cottonwood
"""
import numpy as np
import matplotlib.pyplot as plt
from cottonwood.structure import Structure
from knn_c import KNN
from penguins import Data

n_reps = 5000
n_iter = 70
successes = np.zeros(n_iter)

for i_rep in range(n_reps):

    model = Structure()
    model.add(Data(), "data")
    model.add(KNN(), "knn")

    model.connect("data", "knn")
    model.connect("data", "knn", i_port_tail=1, i_port_head=1)
    for i_iter in range(n_iter):
        model.forward_pass()
        if model.blocks["knn"].target_label == model.blocks["knn"].forward_out:
            successes[i_iter] += 1

percentage = successes * 100 / n_reps
fig = plt.figure()
ax = fig.gca()
ax.plot(percentage, color="#04253a")
ax.set_title(f"k-NN with Palmer Penguins, k = {model.blocks['knn'].k}")
ax.set_xlabel("Iteration")
ax.set_ylabel("Success percentage")
ax.set_ylim(70, 103)
ax.grid()
plt.savefig("learning_curve.png", dpi=300)
plt.show()
